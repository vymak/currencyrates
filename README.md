Currency
============

Get actual currency

Usage:


```
#!php

use Nette\Caching\Cache;
use Nette\Caching\Storages\FileStorage;
use Nette\Caching\Storages\FileJournal;
use Vymakdevel\Currency;

require 'vendor/autoload.php';

// with cache
$journal = new FileJournal(__DIR . '/tmp');
$storage = new FileStorage(__DIR . '/tmp', $journal);
$currency = new Currency(new Cache($storage));

// without cache
$currency = new Currency();

var_dump($currency->getCurrencyRate(Currency::EUR, Currency::UCB_PURCHASE_DEVIZA));
var_dump($currency->getCurrencyRate(Currency::EUR, Currency::UCB_SALE_DEVIZA));
var_dump($currency->eur);

```