<?php

namespace Test;

use Nette\Caching\Cache;
use Nette\Caching\Storages\FileJournal;
use Nette\Caching\Storages\FileStorage;
use Tester\Assert;
use Tester\TestCase;
use Vymakdevel\Currency;

require_once '../vendor/autoload.php';

class CurrencyTest extends TestCase
{

    public function testWithoutCache()
    {
        $obj = new Currency;
        Assert::truthy($obj->eur);
        Assert::truthy($obj->usd);
        Assert::truthy($obj->gbp);
        Assert::truthy($obj->getData());
        Assert::type('float', $obj->eur);
        Assert::type('float', $obj->usd);
        Assert::type('float', $obj->gbp);

        Assert::false($obj->getCurrencyRate('libra'));
        Assert::truthy($obj->getCurrencyRate(Currency::DKK, Currency::UCB_SALE_DEVIZA));
        Assert::type('float', $obj->getCurrencyRate(Currency::DKK, Currency::UCB_SALE_DEVIZA));
    }

    public function testWithCache()
    {
        $dir = __DIR__ . '/tmp';
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $journal = new FileJournal($dir);
        $storage = new FileStorage($dir, $journal);

        $obj = new Currency(new Cache($storage));
        Assert::truthy($obj->eur);
        Assert::truthy($obj->usd);
        Assert::truthy($obj->gbp);
        Assert::truthy($obj->getData());
        Assert::type('float', $obj->eur);
        Assert::type('float', $obj->usd);
        Assert::type('float', $obj->gbp);

        Assert::false($obj->getCurrencyRate('libra'));
        Assert::truthy($obj->getCurrencyRate(Currency::DKK, Currency::UCB_SALE_DEVIZA));
        Assert::type('float', $obj->getCurrencyRate(Currency::DKK, Currency::UCB_SALE_DEVIZA));
    }

}

$test = new CurrencyTest();
$test->run();

