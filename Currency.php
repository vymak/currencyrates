<?php

namespace Vymakdevel;

use Nette\Caching\Cache;
use Nette\InvalidArgumentException;
use Nette\Object;
use Nette\Utils\Validators;

/**
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 * @version 1.1
 * @property-read double $eur
 * @property-read double $usd
 * @property-read double $gbp
 * @property-write mixed $cacheExpire
 * @property-write array $cacheTags
 * @property-write int $cachePriority
 * @property-write boolean $cacheSliding
 */
class Currency extends Object
{

    const
            AUD = 'AUD',
            CAD = 'CAD',
            DKK = 'DKK',
            EUR = 'EUR',
            GBP = 'GBP',
            HRK = 'HRK',
            CHF = 'CHF',
            JPY = 'JPY',
            NOK = 'NOK',
            NZD = 'NZD',
            SEK = 'SEK',
            USD = 'USD';
    const
            UCB_PURCHASE_VALUTA = 'XML_RATE_TYPE_UCB_PURCHASE_VALUTA',
            UCB_SALE_VALUTA = 'XML_RATE_TYPE_UCB_SALE_VALUTA',
            UCB_MIDDLE_VALUTA = 'XML_RATE_TYPE_UCB_MIDDLE_VALUTA',
            CNB_VALUTA = 'XML_RATE_TYPE_CNB',
            UCB_PURCHASE_DEVIZA = 'XML_RATE_TYPE_UCB_PURCHASE_DEVIZA',
            UCB_SALE_DEVIZA = 'XML_RATE_TYPE_UCB_SALE_DEVIZA',
            UCB_MIDDLE_DEVIZA = 'XML_RATE_TYPE_UCB_MIDDLE_DEVIZA';
    const
            URL = 'https://www.unicreditbank.cz/web/exchange_rates_xml.php';

    /** @var Cache */
    private $cache;
    private $useCache = TRUE;
    private $cacheExpire = '1 hour';
    private $cacheTags = array('currency');
    private $cachePriority = 1000;
    private $cacheSliding = FALSE;
    private $xmlData;

    public function __construct(Cache $cache = NULL)
    {
        if (isset($cache)) {
            $this->cache = $cache;
        } else {
            $this->useCache = FALSE;
        }
    }

    /**
     * Zjištění aktuálního kurzu pro měnu
     * @param string $currency
     * @param string $type
     * @return double
     */
    public function getCurrencyRate($currency = self::EUR, $type = self::UCB_MIDDLE_VALUTA)
    {
        $xml = simplexml_load_string($this->getData());

        if (!$xml || !$this->check($currency, $type)) {
            return FALSE;
        }

        foreach ($xml->exchange_rate as $value) {
            if ((string) $value['type'] === $type) {
                foreach ($value->currency as $attr) {
                    if ((string) $attr['name'] === $currency) {
                        return doubleval($attr['rate']);
                    }
                }
            }
        }
        return FALSE;
    }

    public function getData()
    {
        if (!$this->useCache) {
            if (!isset($this->xmlData)) {
                $this->xmlData = Download::synchroneDownloadOneFile(self::URL, 2000, 5000);
            }
            return $this->xmlData;
        }
        return $this->cache->load(__METHOD__, function (& $dp) {
                    $dp = array(
                        Cache::EXPIRATION => $this->cacheExpire,
                        Cache::SLIDING => $this->cacheSliding,
                        Cache::TAGS => $this->cacheTags,
                        Cache::PRIORITY => $this->cachePriority
                    );

                    return Download::synchroneDownloadOneFile(self::URL, 2000, 5000);
                });
    }

    /**
     * Check type and currency
     * @param string $currency
     * @param string $type
     * @return boolean
     */
    private function check($currency, $type)
    {
        $allowedCurrency = array(
            self::AUD,
            self::CAD,
            self::CHF,
            self::DKK,
            self::EUR,
            self::GBP,
            self::HRK,
            self::JPY,
            self::NOK,
            self::NZD,
            self::SEK,
            self::USD
        );

        $allowedType = array(
            self::CNB_VALUTA,
            self::UCB_MIDDLE_DEVIZA,
            self::UCB_MIDDLE_VALUTA,
            self::UCB_PURCHASE_DEVIZA,
            self::UCB_PURCHASE_VALUTA,
            self::UCB_SALE_DEVIZA,
            self::UCB_SALE_VALUTA
        );

        if (!in_array($currency, $allowedCurrency) || !in_array($type, $allowedType)) {
            return FALSE;
        }
        return TRUE;
    }

    public function getEur()
    {
        return $this->getCurrencyRate(self::EUR);
    }

    public function getUsd()
    {
        return $this->getCurrencyRate(self::USD);
    }

    public function getGbp()
    {
        return $this->getCurrencyRate(self::USD);
    }

    public function setCacheExpire($cacheExpire)
    {
        if (!$this->useCache) {
            trigger_error('You cant set cache expire when you no using cache', E_USER_ERROR);
        }
        $this->cacheExpire = $cacheExpire;
    }

    public function setCacheTags(array $cacheTags)
    {
        if (!$this->useCache) {
            trigger_error('You cant set cache tags when you no using cache', E_USER_ERROR);
        }
        $this->cacheTags = $cacheTags;
    }

    public function setCachePriority($cachePriority)
    {
        if (!$this->useCache) {
            trigger_error('You cant set cache expire when you no using cache', E_USER_ERROR);
        }
        if (!Validators::isNumericInt($cachePriority)) {
            throw new InvalidArgumentException('Parameter must be integer');
        }
        $this->cachePriority = $cachePriority;
    }

    public function setCacheSliding($cacheSliding)
    {
        if (!$this->useCache) {
            trigger_error('You cant set cache sliding when you no using cache', E_USER_ERROR);
        }
        $this->cacheSliding = $cacheSliding;
    }

}
